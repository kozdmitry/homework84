const path = require("path");
const express = require("express");
const { nanoid } = require("nanoid");
const Task = require("../models/Task");
const auth = require("../middleware/auth");

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const task = await Task.find();
        res.send(task);
    } catch (e) {
        res.sendStatus(500);
    }
});


router.post("/", auth, async (req, res) => {
    try {
        const task = new Task(req.body);
        await task.save();
        return res.send(task);
    } catch (e) {
        return res.sendStatus(500);
    }
});


router.put('/:id',async (req, res) => {
    try {
        const taskInfo = req.body;
        const task = await Task.findOne({ _id: req.params.id });

        return res.send(taskInfo);
    } catch (e){
        return res.sendStatus(500);
    }

});

router.delete("/:id", auth, async (req, res) => {
    try {
        await Task.findOneAndDelete({ _id: req.params.id });

        return res.send("Task deleted");
    } catch (e) {
        return res.sendStatus(500);
    }
});


module.exports = router;
