const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const tasks = require("./app/tasks");
const users = require ("./app/users");

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/tasks', tasks);
app.use('/users', users);

const port = 8000;


const run = async () => {
    await mongoose.connect('mongodb://localhost/task_list',
        {useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('disconnect');
        callback();
    });
};

run().catch(console.error)



